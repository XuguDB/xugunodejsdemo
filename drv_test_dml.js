
const xugu = require('../build/Release/xugudbjs');
const conn = xugu.createConnection('IP=192.168.2.216;Port=5138;Database=SYSTEM;USER=SYSDBA;PWD=SYSDBA;char_set=GBK');

conn.connect();
conn.query('use DB_NODEJS');

conn.query("drop table ?", ['nodejs_float']);
conn.query("drop table nodejs_double");
conn.query("drop table nodejs_char");
conn.query("drop table nodejs_varchar");
conn.query("drop table nodejs_numeric");
conn.query("drop table nodejs_clob");
conn.query("drop table nodejs_blob");
conn.query("drop table nodejs_datetime");
conn.query("drop table nodejs_time");
conn.query("drop table nodejs_boolean");
conn.query("drop table nodejs_t1");
conn.query("drop table nodejs_t2");
conn.query("drop table nodejs_t3");
conn.query("drop table nodejs_t4");

conn.end();

