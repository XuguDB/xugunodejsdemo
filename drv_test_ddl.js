
const xugu = require('./xugunodejs/xugudbjs.node');
const conn = xugu.createConnection('IP=127.0.0.1;Port=5138;Database=SYSTEM;USER=SYSDBA;PWD=SYSDBA;char_set=GBK');

conn.connect();
conn.query('create database DB_NODEJS;');
conn.query('use DB_NODEJS');
conn.query('create table nodejs_float(c_1 integer, c_2 float);');
conn.query("create table nodejs_double(c_1 integer, c_2 double);");
conn.query("create table nodejs_char(c_1 integer, c_2 char(20));");
conn.query("create table nodejs_varchar(c_1 integer, c_2 varchar);");
conn.query("create table nodejs_numeric(c_1 integer, c_2 numeric(4,1));");
conn.query("create table nodejs_clob(c_1 integer, c_2 clob);");
conn.query("create table nodejs_blob(c_1 integer, c_2 blob);");
conn.query("create table nodejs_datetime(c_1 integer, c_2 datetime);");
conn.query("create table nodejs_time(c_1 integer, c_2 time);");
conn.query("create table nodejs_boolean(c_1 integer, c_2 boolean);");
conn.query("create table nodejs_t1(c_1 integer, c_2 float, c_3 char(10), c_4 varchar)");
conn.query("create table nodejs_t2(c_1 integer, c_2 double, c_3 datetime, c_4 numeric(10,4))");
conn.query("create table nodejs_t3(c_1 integer, c_2 clob, c_3 clob, c_4 clob)");
conn.query("create table nodejs_t4(c_1 integer, c_2 blob, c_3 blob, c_4 blob)");

conn.end();

