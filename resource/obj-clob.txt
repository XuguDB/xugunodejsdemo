===LuaJIT
* ffi.* API Functions
* Home
* LuaJIT
* Download ⇩
* Installation
* Running
* Extensions
* FFI Library
* FFI Tutorial
* ffi.* API
* FFI Semantics
* jit.* Library
* Lua/C API
* Status
* Changes
* FAQ
* Performance
* on x86/x64
* on ARM
* on PPC
* on PPC/e500
* on MIPS
* Wiki »
Mailing List
Sponsors
This page describes the API functions provided by the FFI library in detail. 
It's recommended to read through the introduction and the FFI tutorial first.

Glossary
cdecl — An abstract C type declaration (a Lua string).
ctype — A C type object. This is a special kind of cdata returned by ffi.typeof(). 
It serves as a cdata constructor when called.
cdata — A C data object. It holds a value of the corresponding ctype.
ct — A C type specification which can be used for most of the API functions. 
Either a cdecl, a ctype or a cdata serving as a template type.
cb — A callback object. This is a C data object holding a special function pointer. 
Calling this function from C code runs an associated Lua function.
VLA — A variable-length array is declared with a ? instead of the number of elements, e.g. 
"int[?]". The number of elements (nelem) must be given when it's created.
VLS — A variable-length struct is a struct C type where the last element is a VLA. 
The same rules for declaration and creation apply.
Declaring and Accessing External Symbols
External symbols must be declared first and can then be accessed 
by indexing a C library namespace, which automatically binds the symbol to a specific library.

ffi.cdef(def)
Adds multiple C declarations for types or external symbols (named variables or functions). 
def must be a Lua string. It's recommended to use the syntactic sugar for string arguments as follows:

ffi.cdef[[
typedef struct foo { int a, b; } foo_t;  // Declare a struct and typedef.
int dofoo(foo_t *f, int n);  /* Declare an external C function. */
]]
The contents of the string (the part in green above) must be a sequence of C declarations, 
separated by semicolons. The trailing semicolon for a single declaration may be omitted.

Please note that external symbols are only declared, 
but they are not bound to any specific address, 
yet. Binding is achieved with C library namespaces (see below).

C declarations are not passed through a C pre-processor, yet. 
No pre-processor tokens are allowed, except for #pragma pack. 
Replace #define in existing C header files with enum, static const 
or typedef and/or pass the files through an external C pre-processor (once). 
Be careful not to include unneeded or redundant declarations from unrelated header files.

ffi.C
This is the default C library namespace — note the uppercase 'C'. 
It binds to the default set of symbols or libraries on the target system. 
These are more or less the same as a C compiler would offer by default, 
without specifying extra link libraries.

On POSIX systems, this binds to symbols in the default or global namespace. 
This includes all exported symbols from the executable and 
any libraries loaded into the global namespace. This includes at least libc, 
libm, libdl (on Linux), libgcc (if compiled with GCC), 
as well as any exported symbols from the Lua/C API provided by LuaJIT itself.

On Windows systems, this binds to symbols exported from the *.exe, 
the lua51.dll (i.e. the Lua/C API provided by LuaJIT itself), 
the C runtime library LuaJIT was linked with (msvcrt*.dll), kernel32.dll, user32.dll and gdi32.dll.

clib = ffi.load(name [,global])
This loads the dynamic library given by name and returns a new 
C library namespace which binds to its symbols. On POSIX systems, 
if global is true, the library symbols are loaded into the global namespace, too.

If name is a path, the library is loaded from this path. 
Otherwise name is canonicalized in a system-dependent way 
and searched in the default search path for dynamic libraries:

On POSIX systems, if the name contains no dot, the extension .so 
is appended. Also, the lib prefix is prepended if necessary. 
So ffi.load("z") looks for "libz.so" in the default shared library search path.

Copyright © 2005-2018 · Contact – IMPRESSUM
