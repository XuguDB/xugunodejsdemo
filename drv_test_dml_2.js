const fs = require('fs');
const xugu = require('../build/Release/xugudbjs');
const conn = xugu.createConnection('IP=192.168.2.216;Port=5138;Database=SYSTEM;USER=SYSDBA;PWD=SYSDBA;char_set=GBK');

function getFilesizeInBytes(filename) {
    const stats = fs.statSync(filename)
    const fileSizeInBytes = stats.size
    return fileSizeInBytes
}
conn.connect();
conn.query('use DB_NODEJS');
conn.query('insert into nodejs_float values(2, 0.01)(2, 0.01);');
conn.query("insert into nodejs_double values(2, 0.00001)(2, 0.00001);");
conn.query("insert into nodejs_char values(2, 'xxxx-xxxxxxxx')(2, 'xxxx-xxxxxxxx');");
conn.query("insert into nodejs_varchar values(2, 'xxxx-xxxxxxxxxxxxxx')(2, 'xxxx-xxxxxxxxxxxxx');");
conn.query("insert into nodejs_numeric values(2, '100.0')(2, '100.0');");
conn.query("insert into nodejs_clob values(2, NULL)(2, NULL);");
conn.query("insert into nodejs_blob values(2, NULL)(2, NULL);");
conn.query("insert into nodejs_datetime values(2, '2019-6-17 16:00:00')(2, '2019-6-17 16:30:00');");
conn.query("insert into nodejs_time values(2, '16:00:00')(2, '12:00:00');");
conn.query("insert into nodejs_boolean values(2, true)(2, false);");
conn.query("insert into nodejs_t1 values(2, 0.02, 'xxxxxx', 'xxx-xxxxxxxxxxxxxx-xxxx')(2, 0.01, 'xxxxxx', 'xxx-xxxxxxxxxxxxxx-xxxx')");
conn.query("insert into nodejs_t2 values(2, 3.1415926, '2019-6-17 17:00:00', '66666.6666')(2, 3.1415926, NULL, '66666.6666')");

var obj_clob_path = './resource/obj-clob.txt';
var obj_blob_path = './resource/obj-blob.jpg';
var obj_clob_byte = getFilesizeInBytes(obj_clob_path);
var obj_blob_byte = getFilesizeInBytes(obj_blob_path);
var obj_clob_buff = new Buffer.alloc(obj_clob_byte);
var obj_blob_buff = new Buffer.alloc(obj_blob_byte);

obj_clob_buff = fs.readFileSync(obj_clob_path);
obj_blob_buff = fs.readFileSync(obj_blob_path);

obj_clobs = [obj_clob_buff, obj_clob_buff, obj_clob_buff];
obj_blobs = [obj_blob_buff, obj_blob_buff];

//conn.query("insert into nodejs_t3(c_1 integer, c_2 clob, c_3 clob, c_4 clob)");
//conn.query("insert into nodejs_t3 values(1, ?, ?, ?)", obj_clobs, function(err, results, fields));
//conn.query("insert into nodejs_t4(c_1 integer, c_2 blob, c_3 blob, c_4 blob)");
conn.query("insert into nodejs_t4 values(1, ?, ?, NULL)", obj_blobs, function(err, results, fields){
    if(err) {
        throw err;
    }

    console.log(results);
    console.log(fields);
})


conn.end();

